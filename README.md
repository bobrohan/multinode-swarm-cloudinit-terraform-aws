# Multi Node Swarm
## cloudinit, terraform, aws

Multi node swarm, single master, single worker

# Roadmap

- Join worker to swarm via cloudinit

## Prereqs 

Add the following to `terraform.tfvars` 

```
accesskey=
secretkey=
dreamteam-pubkey=
dreamteam-dburi=
```

## One command deploy
 
`terraform apply --auto-approve`

## Post Deploy
`docker swarm join --token <token> 10.0.1.4:2377`