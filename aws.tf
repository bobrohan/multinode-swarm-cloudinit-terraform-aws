provider "aws" {
  region      = "eu-west-2"
  access_key = "${var.accesskey}"
  secret_key = "${var.secretkey}"
}

resource "aws_key_pair" "dreamteam" {
  key_name   = "dreamteam"
  public_key = "${var.dreamteam-pubkey}"
}

output "dreamteam-manager-ip" {
  value = "${aws_eip.dreamteam-manager.public_ip}"
}

output "dreamteam-worker-ip" {
  value = "${aws_eip.dreamteam-worker.public_ip}"
}