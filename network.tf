resource "aws_vpc" "dreamteam" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

resource "aws_internet_gateway" "dreamteam" {
  vpc_id = "${aws_vpc.dreamteam.id}"
}

resource "aws_route_table" "dreamteam" {
  vpc_id = "${aws_vpc.dreamteam.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.dreamteam.id}"
  }
}

resource "aws_subnet" "dreamteam-manager" {
  cidr_block = "10.0.1.0/28"
  vpc_id = "${aws_vpc.dreamteam.id}"
}

resource "aws_route_table_association" "dreamteam-manager" {
  subnet_id      = "${aws_subnet.dreamteam-manager.id}"
  route_table_id = "${aws_route_table.dreamteam.id}"
}

resource "aws_subnet" "dreamteam-worker" {
  cidr_block = "10.0.2.0/28"
  vpc_id = "${aws_vpc.dreamteam.id}"
}

resource "aws_route_table_association" "dreamteam-worker" {
  subnet_id      = "${aws_subnet.dreamteam-worker.id}"
  route_table_id = "${aws_route_table.dreamteam.id}"
}