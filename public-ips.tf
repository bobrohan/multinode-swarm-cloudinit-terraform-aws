resource "aws_eip" "dreamteam-manager" {
  instance = "${aws_instance.dreamteam-manager.id}"
  vpc      = true
}

resource "aws_eip" "dreamteam-worker" {
  instance = "${aws_instance.dreamteam-worker.id}"
  vpc      = true
}